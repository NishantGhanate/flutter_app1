import 'package:flutter/material.dart';
//import 'dart:io';
//import 'dart:async';
//import 'package:flutter/services.dart';
//import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
//import 'package:path_provider/path_provider.dart';
//import 'package:flutter_string_encryption/flutter_string_encryption.dart';
//import 'package:encrypt/encrypt.dart';
//import './dailog.dart';

import 'package:random_string/random_string.dart';
void main() {

//  print(randomBetween(10,20)); // some integer between 10 and 20
//  print(randomNumeric(4)); // sequence of 4 random numbers i.e. 3259
  print(randomString(32)); // random sequence of 10 characters i.e. e~f93(4l-
  print(randomAlpha(32)); // random sequence of 5 alpha characters i.e. aRztC
//  print(randomAlphaNumeric(10)); // random sequence of 10 alpha numeric i.e. aRztC1y32B

  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute(),
  ));
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute()),
            );
          },
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}