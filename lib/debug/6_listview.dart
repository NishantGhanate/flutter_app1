import 'package:flutter/material.dart';
//import 'dart:io';
//import 'dart:async';
//import 'package:flutter/services.dart';
//import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
//import 'package:path_provider/path_provider.dart';
//import 'package:flutter_string_encryption/flutter_string_encryption.dart';
//import 'package:encrypt/encrypt.dart';

//void main() => runApp(MyApp());

void main() {
  runApp(MyApp(
    items: List<String>.generate(35, (i) => "Item $i"),
  ));
}

class MyApp extends StatelessWidget {
  final List<String> items;


  MyApp({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final title = 'Long List';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text('${items[index]}'),
              onTap: () => print('${items[index]}'),
            );
          },
        ),
      ),
    );
  }
}

//
//class _MyHomePageState extends State<MyHomePage> {
//  int _counter = 0;
//
//  void _incrementCounter() {
//    setState(() {
//      // This call to setState tells the Flutter framework that something has
//      // changed in this State, which causes it to rerun the build method below
//      // so that the display can reflect the updated values. If we changed
//      // _counter without calling setState(), then the build method would not be
//      // called again, and so nothing would appear to happen.
//      _counter++;
//    });
//  }
