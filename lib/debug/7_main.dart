import 'package:flutter/material.dart';
//import 'dart:io';
//import 'dart:async';
//import 'package:flutter/services.dart';
//import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
//import 'package:path_provider/path_provider.dart';
//import 'package:flutter_string_encryption/flutter_string_encryption.dart';
//import 'package:encrypt/encrypt.dart';
//import './dailog.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //DialogHandle  dialog = new DialogHandle();

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),

      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround ,
        children: <Widget>[
          Row(
            mainAxisAlignment:MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                //onPressed: ()=> dialog.information(context,'This is a title','Fluter app is soccery'),
                child: Text("REEEEEEEEEEEEEEEE"),
              )
            ],
          ),
          Row(
            mainAxisAlignment:MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () async{
                  //dialog.waiting(context,'This is a title','Fluter app is soccery');
                  // close automatically in 2 sec
                  await Future.delayed(Duration(seconds: 2));
                  Navigator.pop(context);
                },
                child: Text("WEEEEEEEEEEEEEEEE"),
              )
            ],
          ),
          Row(
            mainAxisAlignment:MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  //dialog.confirm(context,'This is a title','Fluter app is soccery');
                  // close automatically in 2 sec

                },
                child: Text("AEEEEEEEEEEEEEEEE"),
              )
            ],
          ),
        ],
      ),
    );
  }
}
