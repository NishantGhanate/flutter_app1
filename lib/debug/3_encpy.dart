import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'package:encrypt/encrypt.dart';
import 'package:password_hash/password_hash.dart';
import 'dart:convert'; // for the utf8.encode method
import 'package:random_string/random_string.dart'; // Random string
//void main() => runApp(MyApp());

void main() {
  // key len should be 128 bit
  var alpha;
  print(randomString(8)); // random sequence of 10 characters i.e. e~f93(4l-
  alpha = randomAlpha(8);
  //print(alpha); // random sequence of 5 alpha characters i.e. aRztC
  print(randomAlphaNumeric(8)); // random sequence of 10 alpha numeric i.e. aRztC1y32B


  var generator = new PBKDF2();
  var salt = 'harcoded';
  var hash = generator.generateKey("my safe password 123 ^", salt, 1000, 22);
  print(hash);
  print(hash.length);
  var base62 = base64.encode(hash);
  print(base62);
  print(base62.length);


  final key = 'my32lengthsupersecretnooneknows1';
  print(key.length);
  final encrypter = new Encrypter(new AES(key));
  final plainText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ........';

  final encryptedText = encrypter.encrypt(plainText);
  final decryptedText = encrypter.decrypt(encryptedText);

  print(encryptedText); // db066ce180f62f020617eb720b891c1efcc48b217cb83272812a8efe3b30e7eae4373ddcede4ea77bdae77d126d95457b3759b1983bf4cb4a6a5b051a5690bdf
  print(decryptedText); // Lorem ipsum dolor sit amet, consectetur adipiscing elit ........


}