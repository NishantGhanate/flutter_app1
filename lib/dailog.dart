import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

class DialogHandle{
  TextEditingController userInputController = TextEditingController();
  String input;

  information(BuildContext context,String title,String description){
    return showDialog(
        context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description)
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: ()=> Navigator.pop(context),
                child: Text("Ok"),
              )
            ],
          );
      }
    );
  }

  waiting(BuildContext context,String title,String description){
    return showDialog(
        context: context,
        barrierDismissible: false,

        builder: (BuildContext context){
          return AlertDialog(


            title: Text(title),
            content: SingleChildScrollView(
              
              child: ListBody(
                children: <Widget>[

                  Text(description)
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: ()=> Navigator.pop(context),
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }

  _write() async {
      final directory = await getApplicationDocumentsDirectory();
      var path = directory.path;


//      final myDir = new Directory('$path/encpy');
//      myDir.exists().then((isThere) {
//        if(isThere){
//          path = '$path/encpy';
//          print(path);
//          print('exists');
//
//        }
//        else{
//         print('non-existent');
//         new Directory('$path/encpy').create(recursive: true)
//         // The created directory is returned as a Future.
//             .then((Directory directory) {
//           print(directory.path);
//           path = directory.path;
//         });
//        }
//      });

      // re write stuff
      var file = new File('$path/encpy/counter.txt');

      new File('$path/encpy/counter.txt').readAsString().then((String contents) {
        print(contents);
      });

      var sink = file.openWrite();
      sink.write('FILE ACCESSED ${new DateTime.now()}\n');
      // Close the IOSink to free system resources.
      sink.close();

     path = '$path/encpy';
      var systemTempDir = Directory(path);

      // List directory contents, recursing into sub-directories,
      // but not following symbolic links.
      systemTempDir.list(recursive: true, followLinks: false)
          .listen((FileSystemEntity entity) {
        print(entity.path);
      });

      return directory.path;
  }

  _confirmResult(bool isYes , BuildContext context){
    if(isYes){
      print("HELL YES!" );
      print(userInputController.text);
      _write();
      Navigator.pop(context);
    }
    else{
      print("HELL NO!");
      print(input);

      Navigator.pop(context);
    }
  }

  confirm(BuildContext context,String title,String description){

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description),
                  TextField(
                    onChanged: (text) {
                      print("First text field: $text");
                    },
                      controller: userInputController,
                  ),

                ],

              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: ()=> _confirmResult(true,context),
                child: Text("Save"),
              ),
              FlatButton(
                onPressed: ()=> _confirmResult(false, context),
                child: Text("Cancel"),
              )
            ],
          );
        }
    );
  }


  test(BuildContext context,String title,String description){

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description),
                  TextField(
                    onChanged: (text) {
                      print("First text field: $text");
                    },
                    controller: userInputController,
                  ),

                ],

              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: ()=> _confirmResult(true,context),
                child: Text("Save"),
              ),
              FlatButton(
                onPressed: ()=> _confirmResult(false, context),
                child: Text("Cancel"),
              )
            ],
          );
        }
    );
  }

}